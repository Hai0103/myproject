const {Provider, connect} = ReactRedux;
const {createStore, applyMiddleware} = Redux;
const thunk = ReduxThunk.default;

function fetchPostsRequest(){
  return { type: "FETCH_REQUEST" }
}

function fetchPostsSuccess(payload) {
  return {
    type: "FETCH_SUCCESS",
    payload
  }
}

function fetchPostsError() {
  return {
    type: "FETCH_ERROR"
  }
}



function fetchPostsAPI() {
	return (dispatch) => {
  	dispatch(fetchPostsRequest());
    return fetchPosts().then(([response, json]) =>{
    	if(response.status === 200){
      	dispatch(fetchPostsSuccess(json))
      }
      else{
      	dispatch(fetchPostsError())
      }
    })
  }
}

function fetchPosts() {
  const URL = "https://jsonplaceholder.typicode.com/posts";
  return fetch(URL, { method: 'GET'})
     .then( response => Promise.all([response, response.json()]));
}


function mapStateToProps(state){
	return { posts: state.posts }
}
function mapDispatchToProps(dispatch) {
    return{ fetchPostsAPI: () => dispatch(fetchPostsAPI ())}
}

const reducer = (state = {}, action) => {
  let posts = state.posts;
  console.log('action type', action.type)
  switch (action.type) {  	
    case "FETCH_REQUEST":
      return state;
    case "FETCH_SUCCESS": 
      return {...state, posts: action.payload};
    default:
      return state;
  }
} 

let Container = connect(mapStateToProps, mapDispatchToProps)(HaiNMForm);
class HaiNMForm extends React.Component {
	constructor (props, context) {
    super(props, context);
    this.props.fetchPostsAPI();
    this.onaddPost = this.onaddPost.bind(this);    
  }
  
  componentDidMount(){
  	this.props.fetchPostsAPI();
    this.setState({
  		nextPostId: 101
    })
  }
  
  onaddPost() {
    let post = {
		id : this.state.nextPostId++,
		title: "", body: "", userId: "" };
    this.props.posts.push(post);
    this.setState({
  		postList: this.props.posts
    })
  }
  
	render(){
	  const posts = this.props.posts;
 		let list = '';
    if (posts != undefined){
      list = posts.map((post, index) =>{
        return (<tr key={post.id}>
          <td>{post.id}</td>
          <td><input type="text" value={post.title} /></td>
          <td><input type="text" value={post.body} /></td>
          <td><input type="text" value={post.userId} /></td>          
        </tr>);
      }); 
 		}
    return (
	<div>
      <table border="1">
        <thead>
          <th>ID</th>
          <th>Title</th>
          <th>Body</th>
          <th>User ID</th>          
        </thead>
        <tbody>
          {list}
        </tbody>
      </table>
      <br/>
      <button onClick={this.onaddPost}>
        Add Post
      </button>
    </div>);
  }
}
const store = createStore(
    reducer,
    applyMiddleware(thunk)
);
ReactDOM.render(
    <Provider store={store}>
        <Container/>
    </Provider>,
    document.getElementById('hainm')
);
