function ex2() {
  const inputArray = [1, 4, 2, 3, 5];
  let sum;
  if(inputArray === undefined){
    sum = undefined;  
  } else if(inputArray.length == 0){
  	sum = 0;
  } else {
    inputArray.sort((a, b) => b - a);
    sum = inputArray[0] + inputArray[1];    
  }
};

const assert = (fun, input, expected) => {
 return fun(input) === expected ? 'passed' : `failed on input=${input}. expected ${expected}, but got ${fun(input)}`;
}

let testCases = [
 {input: [1,4,2,3,5,6,8,7,9], expected: 45},
 {input: undefined, expected: undefined},
 {input: [null], expected: 0},
 {input: [], expected: 0},
];

let testResult = testCases.map(test => assert(ex2, test.input, test.expected))

console.log(testResult);