function ex1() {
  const inputArray = ['a', 'ab', 'abc', 'cd', 'def'];
  let maxCount;  
  let newArray = [];
  let countArr = [];
  let preResult = [];
  let finalResult = [];
  if(inputArray == undefined) {
    finalResult = undefined
  } 
  else 
  {
    inputArray.forEach( i => {
      let count = 0;
      if(i == null || i == undefined){
        return i;
      } 
      else 
      {              
        inputArray.forEach( x => {
          if(x == null || x == undefined){
            return x;
          } 
          else 
          {
            if(i.length === x.length){
              count = count + 1;
            }
          }
        })
      }

      countArr.push(count);
      if(count != 0) {
        newArray.push({value: i, count: count});
      }
    });
  }
  maxCount = countArr.sort(function(a,b){return b-a;})[0];
  
  newArray.forEach( y => {
  	if(y.count == maxCount){
      preResult.push(y);
    }
  })

  let length = []; 
  preResult.forEach( z => {
  	length.push(z.value.length);
  })
  let maxLen = length.sort(function(a,b){return b-a;})[0];
  
  preResult.forEach( res => {
  	if(res.value.length == maxLen) {
      finalResult.push(res.value);
    }
  })
};
ex1();

const assert = (fun, input, expected) => {
 return fun(input) === expected ? 'passed' : `failed on input=${input}. expected ${expected}, but got ${fun(input)}`;
}

let testCases = [
  {input: ['a', 'ab', 'hai', 'cd', 'anh'], expected: [ 'hai', 'anh' ]},
  {input: ['a', 'ha', 'c', 'ah', 'def'], expected: ['ha', 'ah']},
  {input: undefined, expected: undefined},
  {input: [], expected: []},
  {input: [null, null], expected: []},
];

let testResult = testCases.map(test => assert(ex1, test.input, test.expected))

console.log(testResult);
